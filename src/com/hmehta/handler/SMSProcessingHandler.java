package com.hmehta.handler;

import java.util.Map;
import java.util.HashMap;
import javax.mail.Message;
import javax.mail.Session;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.mail.Transport;
import com.hmehta.model.SMSRequest;
import com.hmehta.model.SMSResponse;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class SMSProcessingHandler implements RequestHandler<SMSRequest, SMSResponse> {

	private static final String FROM_EMAIL = "hmehta.email@gmail.com";

	@Override
	public SMSResponse handleRequest(SMSRequest input, Context context) {
		SMSResponse smsResponse = new SMSResponse();

		context.getLogger().log("Function=" + context.getFunctionName() + ", " + input.getClass() + "=" + input.toString());

		String fromEmail = "AWS Lambda Test<hmehta.email@gmail.com>";
		String subject = "AWS Lambda Test";

		final Properties awsProperties = new Properties();
		try {
			InputStream stream = null;
			stream = this.getClass().getResourceAsStream("aws.properties");
			if (stream != null) {
				awsProperties.load(stream);
			} else {
				stream = this.getClass().getResourceAsStream("../../../aws.properties");
				awsProperties.load(stream);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		Properties properties = System.getProperties();
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.port", awsProperties.getProperty("aws.smtpPort"));
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.starttls.required", "true");

		Session mailSession = Session.getDefaultInstance(properties);
		Transport transport = null;
		try {
			String toEmail = input.getCellPhoneNumber() + getServiceProviderEmail(input.getServiceProvider());
			MimeMessage message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(FROM_EMAIL));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
			message.setSubject(subject);
			InternetAddress[] addresses = new InternetAddress[1];
			addresses[0] = new InternetAddress(fromEmail);
			message.setReplyTo(addresses);
			message.setContent(input.getSmsText(), "text/plain");
			transport = mailSession.getTransport();
			context.getLogger().log("Sending SMS TEXT via SES SMTP interface. Port=" + awsProperties.getProperty("aws.smtpPort") + "...");
			transport.connect(awsProperties.getProperty("aws.smtpHost"), awsProperties.getProperty("aws.smtpUsername"),
					awsProperties.getProperty("aws.smtpPassword"));
			transport.sendMessage(message, message.getAllRecipients());
			context.getLogger().log("Email sent.");
			smsResponse = new SMSResponse("SUCCESS");
			context.getLogger().log("Done");
		} catch (MessagingException mex) {
			context.getLogger().log("Exception closing the Transport." + mex.toString());
			smsResponse = new SMSResponse("FAIL");
		} finally {
			try {
				if (transport != null) {
					transport.close();
				}
			} catch (MessagingException mex) {
				context.getLogger().log("Exception closing the Transport." + mex.toString());
				smsResponse = new SMSResponse("FAIL");
			}
		}

		return smsResponse;
	}

	private String getServiceProviderEmail(String provider) {
		Map<String, String> providers = new HashMap<String, String>();
		providers.put("ATT", "@mms.att.net");
		providers.put("SPRINT", "@messaging.sprintpcs.com");
		providers.put("VERIZON", "@vtext.com");
		providers.put("TMOBILE", "@tmomail.net");		

		return providers.get(provider);
	}
}