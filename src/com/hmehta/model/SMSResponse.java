package com.hmehta.model;

public class SMSResponse {

	private String status = null;
	private String sid = null;

	public SMSResponse() {
	}

	public SMSResponse(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

}