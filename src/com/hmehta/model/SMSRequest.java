package com.hmehta.model;

public class SMSRequest {

	private String cellPhoneNumber = null;
	private String serviceProvider = null;
	private String smsText = null;

	public SMSRequest() {

	}

	public SMSRequest(String cellPhoneNumber, String smsText) {
		this.cellPhoneNumber = cellPhoneNumber;
		this.smsText = smsText;
	}

	public String getSmsText() {
		return smsText;
	}

	public void setSmsText(String smsText) {
		this.smsText = smsText;
	}

	public String getCellPhoneNumber() {
		return cellPhoneNumber;
	}

	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SMSRequest [cellPhoneNumber=").append(cellPhoneNumber).append(", serviceProvider=").append(serviceProvider)
				.append(", smsText=").append(smsText).append("]");
		return builder.toString();
	}

}