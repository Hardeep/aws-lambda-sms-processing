package com.hmehta;

import org.junit.Test;
import java.io.IOException;
import org.junit.BeforeClass;

import com.hmehta.handler.SMSProcessingHandler;
import com.hmehta.model.SMSRequest;
import com.hmehta.model.SMSResponse;
import com.amazonaws.services.lambda.runtime.Context;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class SMSProcessingHandlerTest {

	private static SMSRequest smsRequest;

	@BeforeClass
	public static void createInput() throws IOException {
		smsRequest = new SMSRequest();
		smsRequest.setCellPhoneNumber("9143309609");
		smsRequest.setSmsText("This is a test sms.");
		smsRequest.setServiceProvider("ATT");
	}

	private Context createContext() {
		TestContext context = new TestContext();

		context.setFunctionName("com.hmehta.SMSProcessingHandler::handleRequest");

		return context;
	}

	@Test
	public void testSMSProcessingHandler() {
		Context context = createContext();

		SMSProcessingHandler handler = new SMSProcessingHandler();
		SMSResponse smsResponse = handler.handleRequest(smsRequest, context);
		if (smsResponse != null) {
			System.out.println(smsResponse.toString());
		}
	}

}